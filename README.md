The analyses presented in this repository are related to the following publication:
Moreira Soares Oliveira B, Durbeej M, Holmberg J (2017) Absence of microRNA-21 does not reduce muscular
dystrophy in mouse models of LAMA2-CMD. PLoS ONE 12(8): e0181950. https://doi.org/10.1371/journal.pone.0181950
Copyright: © 2017 Moreira Soares Oliveira et al. This is an open access article distributed under the terms of
the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any
medium, provided the original author and source are credited.

As of January 1, 2018, there seems to be a bug in GitLab in which only the last plot of and IPython notebook will
be drawn.

