# Plotting inline
% matplotlib inline

# Import the necessary modules
import os
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import statsmodels as sm
from statsmodels.sandbox.stats.multicomp import MultiComparison

# We'll first analyse the body weight data from our two mouse models, dy3K and dy2J
# Read the data
wgt3k = pd.read_table('kroppvikt_3k.tsv', header = 0, decimal = ',')
# Explicitly setting the genotype as a category
# DKO means double knock-out, which in the plots is labelled as dy3K/miR-21
gen3k = ['wt', 'mir-21', 'dy3k', 'dko']
ticks3k = ['WT', 'miR-21', 'dy3K', 'dy3K/miR-21']
wgt3k.genotyp = wgt3k.genotyp.astype('category', categories = gen, ordered = True)

# Crete a multiple comparison object for further statistical testing
mcv3 = MultiComparison(wgt3k.vikt, wgt3k.genotyp)
# Compare using t-test
print(mcv3.allpairtest(sp.stats.ttest_ind)[0])
# Visualise the data
sns.barplot(x = 'genotyp', y = 'vikt', data = wgt3k)
plt.ylabel('Body weight (g)')
plt.xlabel('Genotype')
plt.title('Body weight - dy3K')
plt.xticks(range(4), ticks3k)
plt.tight_layout()
plt.show()

# Now the same thing for dy2J mice
wgt2j = pd.read_table('kroppvikt_2j.tsv', header = 0, decimal = ',')
gen2j = ['wt', 'mir-21', 'dy2j', 'dko']
ticks2j = ['WT', 'miR-21', 'dy2J', 'dy2J/miR-21']
wgt2j.genotyp = wgt2j.genotyp.astype('category', categories = gen2j, ordered = True)
mcv2 = MultiComparison(wgt2j.vikt, wgt2j.genotyp)
print(mcv2.allpairtest(sp.stats.ttest_ind)[0])
sns.barplot(x = 'genotyp', y = 'vikt', data = wgt2j)
plt.ylabel('Body weight (g)')
plt.xlabel('Genotype')
plt.title('Body weight - dy2J')
plt.xticks(range(4), ticks2j)
plt.tight_layout()
plt.show()

# Collagen quantification by absorbance
col3k = pd.read_table('Fibros/abs_3k.tsv', header = 0, index_col = 0, decimal = ',', usecols = ['id', 'genotyp', 'pct_koll'])
col3k.genotyp = col3k.genotyp.astype('category', categories = gen3k, ordered = True)
mcc3 = MultiComparison(col3k.pct_koll, col3k.genotyp)
print(mcc3.allpairtest(sp.stats.ttest_ind)[0])
sns.barplot(x = 'genotyp', y = col3k.pct_koll * 100, data = col3k)
plt.ylabel('% tissue weight')
plt.xlabel('Genotype')
plt.title('Collagen quantification by absorbance- dy3K')
plt.xticks(range(4), ticks3k)
plt.tight_layout()
plt.show()

# Same thing for dy2J mice
col2j = pd.read_table('Fibros/abs_2j.tsv', header = 0, index_col = 0, decimal = ',', usecols = ['id', 'genotyp', 'pct_koll'])
col2j.genotyp = col2j.genotyp.astype('category', categories = gen2j, ordered = True)
mcc2 = MultiComparison(col2j.pct_koll, col2j.genotyp)
print(mcc2.allpairtest(sp.stats.ttest_ind)[0])
sns.barplot(x = 'genotyp', y = col2j.pct_koll * 100, data = col2j)
plt.ylabel('% tissue weight')
plt.xlabel('Genotype')
plt.title('Collagen quantification by absorbance- dy2J')
plt.xticks(range(4), ticks2j)
plt.tight_layout()
plt.show()

# We've also quantified it by hydroxyproline assay
hyd3k = pd.read_table('Fibros/Hydroxiprolin/hydroxiprolin_3k_Johan.tsv', header = 0, index_col = 0, decimal = ',')
hyd3k.genotyp = hyd3k.genotyp.astype('category', categories = gen3k, ordered = True)
mch3 = MultiComparison(hyd3k.regress_värde, hyd3k.genotyp)
print(mch3.allpairtest(sp.stats.ttest_ind)[0])
sns.barplot(x = 'genotyp', y = 'regress_värde', data = hyd3k)
plt.ylabel('µg col/µg tissue')
plt.xlabel('Genotype')
plt.title('Collagen quantification by hydroxyproline - dy3K')
plt.xticks(range(4), ticks3k)
plt.tight_layout()
plt.show()

# dy2J mice
hyd2j = pd.read_table('Fibros/Hydroxiprolin/hydroxiprolin_2j_Johan.tsv', header = 0, index_col = 0, decimal = ',')
hyd2j.genotyp = hyd2j.genotyp.astype('category', categories = gen2j, ordered = True)
mch2 = MultiComparison(hyd2j.regress_värde, hyd2j.genotyp)
print(mch2.allpairtest(sp.stats.ttest_ind)[0])
sns.barplot(x = 'genotyp', y = 'regress_värde', data = hyd2j)
plt.ylabel('µg col/µg tissue')
plt.xlabel('Genotype')
plt.title('Collagen quantification by hydroxyproline - dy2J')
plt.xticks(range(4), ticks2j)
plt.tight_layout()
plt.show()

# Central nucleation
cn3k = pd.read_table('centralakärnor_3k.tsv', header = 0)
cn3k.genotyp = cn3k.genotyp.astype('category', categories = gen3k[2:], ordered = True)

# Given that we only have 2 groups here we can use a t-test right away
print(sp.stats.ttest_ind(cn3k.procent[:4], cn3k.procent[4:]))
sns.stripplot(x = 'genotyp', y = 'procent', data = cn3k, jitter = True, size = 8)
plt.ylabel('Percent')
plt.xlabel('Genotype')
plt.title('Central nucleation - dy3K')
plt.xticks([0, 1], ticks3k[2:])
plt.tight_layout()
plt.show()

# dy2J
cn2j = pd.read_table('centralakärnor_2j.tsv', header = 0, decimal = ',')
cn2j.genotyp = cn2j.genotyp.astype('category', categories = gen2j[2:], ordered = True)
print(sp.stats.ttest_ind(cn2j.procent[:3], cn2j.procent[3:]))
sns.stripplot(x = 'genotyp', y = 'procent', data = cn3k, jitter = True, size = 8)
plt.ylabel('Percent')
plt.xlabel('Genotype')
plt.title('Central nucleation - dy2J')
plt.xticks([0, 1], ticks2j[2:])
plt.tight_layout()
plt.show()

# Finally, we have grip strength data from dy2J mice
grip = pd.read_table('Grepp/styrka_2j.tsv', header = 0, decimal = ',')
grip.genotyp = grip.genotyp.astype('category', categories = gen2j, ordered = True)
# There is both absolute and normalised strength; let's start with absolute values
mcga = MultiComparison(grip.styrka, grip.genotyp)
print(mcga.allpairtest(sp.stats.ttest_ind)[0])
# Body weight-normalised strength
mcgn = MultiComparison(grip.norm, grip.genotyp)
print(mcgn.allpairtest(sp.stats.ttest_ind)[0])

# Visualise
y0 = grip[grip.genotyp == 'dy2j'].styrka
y1 = grip[grip.genotyp == 'dko'].styrka
y2 = grip[grip.genotyp == 'dy2j'].norm
y3 = grip[grip.genotyp == 'dko'].norm
x0 = np.array([1])
x1 = np.array([1.2])

fig, ax = plt.subplots(1, 2, sharey = False)
ax[0].plot(x0.repeat(len(y0)), y0, 'mo', x1.repeat(len(y1)), y1, 'ro', ms = 8)
ax[0].set_xlim(0.9, 1.3)
ax[0].set_xticks((x0, x1))
ax[0].set_xticklabels(ticks2j[2:])
ax[0].set_ylabel('Grip strength (kgf)')
ax[0].set_title('Absolute strength')

ax[1].plot(x0.repeat(len(y2)), y2, 'mo', x1.repeat(len(y3)), y3, 'ro', ms = 8)
ax[1].set_xlim(0.9, 1.3)
ax[1].set_xticks((x0, x1))
ax[1].set_xticklabels(ticks2j[2:])
ax[1].set_ylabel('Grip strength (kgf/g)')
ax[1].set_title('Normalised strength')
plt.tight_layout()
plt.show()
