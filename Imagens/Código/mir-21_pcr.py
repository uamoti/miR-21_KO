os.chdir('/home/muskelbiologi/ownCloud/Doktorat/miRNA/miR-21/qPCR')
pcr = pd.read_table('qPCR/miR-21_miR-93_2017-06-22_andra_repl2_corrected.tsv',
                    skiprows=0, decimal=',', header=0)
leg = [r'$dy^{3K}$', r'$dy^{3K}$/miR-21']
xticks = ['miR-21', 'miR-93']
pal = sns.color_palette(palette=['m', 'r'], n_colors=2)

sns.pointplot(x='mål', y='medel_cp', hue='genotyp', data=pcr, join=False, color='grey',
              capsize=0.2)
sns.stripplot(x='mål', y='medel_cp', hue='genotyp', split=True, jitter=True,
              size=8, palette=pal, alpha=0.85, data=pcr)
plt.title('qPCR', fontsize=12)
plt.xlabel('Target gene')
plt.xticks((0, 1), xticks, fontsize=12)
plt.ylabel('Average Cp', fontsize=12)
plt.legend(leg, loc=4)

fig = plt.gcf()
fig.dpi = 300
fig.set_size_inches(4, 5)

plt.tight_layout()
plt.show()
