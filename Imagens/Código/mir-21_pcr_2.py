width = 0.2
# Raw values from the PCR run
dko = 0.01
dy3k = np.array([0.768, 1.214, 1.073])

plt.bar((0.5, 1), (dy3k.mean(), dko), width, color=['m', 'r'], yerr=(dy3k.std(), 0))
plt.xticks((0.5, 1), ('$dy^{3K}/dy^{3K}$', '$dy^{3K}$/miR-21'), fontsize=12)
plt.ylabel('miR-21 expression\n(fold change vs. $dy^{3K}/dy^{3K}$)', fontsize=10)
plt.text(1, 0.011, '***', {'size': 16}, va='center', ha='center')

fig = plt.gcf()
fig.dpi = 300
fig.set_size_inches(3, 5)

plt.tight_layout()
plt.show()
