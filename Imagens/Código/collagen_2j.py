import os
import numpy as np
import scipy as sp
import pandas as pd
import pandas.api.types as pdt
import matplotlib.pyplot as plt
import matplotlib.patches as mp
import seaborn as sns

path = '~/ownCloud/Doutoramento/microRNA/miR-21/Dados/'
os.chdir(os.path.expanduser(path))

ab = pd.read_table('absorbance_2j.tsv', header=0, decimal=',',
                   usecols=['genotype', 'pct_coll'])
ab = ab[(ab.genotype == 'dy2j') | (ab.genotype == 'dko')]
xticks = [r'$dy^{2J}/dy^{2J}$', r'$dy^{2J}$/miR-21']
pal = sns.color_palette(palette=['m', 'r'], n_colors=2)

sns.pointplot(x='genotype', y='pct_coll', data=ab, join=False, color='k',
              markers=['_', '_'], scale=2, capsize=0.05)
sns.stripplot(x='genotype', y='pct_coll', data=ab2, size=6, palette=pal,
              alpha=0.8, jitter=True)
plt.ylim(0, 0.05)
plt.title('Collagen quantification\nby absorbance', fontsize=12)
plt.ylabel('µg collagen/mg protein', fontsize=12)
plt.xlabel('')
plt.xticks((0, 1), xticks, fontsize=12)
#plt.annotate('', [0, 4.13], [1, 4.13], arrowprops={'arrowstyle': '-',
#            'connectionstyle': 'bar,fraction=0.1', 'lw': 1})
#plt.text(0.5, 4.15, '*', {'size': 16}, va='center', ha='center')
plt.annotate('', [0, 0.042], [1, 0.042], arrowprops={'arrowstyle': '-',
            'connectionstyle': 'bar,fraction=0.1', 'lw': 1})
plt.text(0.5, 0.046, '*', {'size': 16}, va='center', ha='center')

fig = plt.gcf()
fig.dpi = 300
fig.set_size_inches(4, 5)
plt.tight_layout()
plt.show()
