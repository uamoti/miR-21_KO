import os
import numpy as np
import scipy as sp
import pandas as pd
import pandas.api.types as pdt
import matplotlib.pyplot as plt
import matplotlib.patches as mp
import seaborn as sns

path = '~/ownCloud/Doutoramento/microRNA/miR-21/'
os.chdir(os.path.expanduser(path))

tgf = pd.read_table('Dados/tgf_qpcr_2j.tsv', decimal=',', header=0)
cats = pdt.CategoricalDtype(categories=['wt', 'mir-21', 'dy2j', 'dko'], ordered=True)
tgf.genotype = tgf.genotype.astype(cats)
grp = tgf.groupby('genotype').fold_change
n = grp.apply(len)
c = ['g', 'y', 'm', 'r']
font = {'size': 12}
xticks = ['WT', 'miR-21 ko', '$dy^{2J}/dy^{2J}$', '$dy^{2J}$/miR-21']

err = grp.sem()
grp.mean().plot.bar(color=c, yerr=err)
plt.ylabel('$Tgfb$ expression'+'\n(fold change vs. WT)', fontsize=12)
plt.ylim(0, 2.65)
plt.xticks(range(4), xticks, fontsize=12, rotation=45)
plt.xlabel('')
plt.text(2, 2.55, '**', {'size': 16}, va='center', ha='center')

for i in range(4):
    plt.text(i, 0.1, str(n[i]), va='center', ha='center', fontdict=font)

fig = plt.gcf()
fig.dpi = 300
fig.set_size_inches(4, 6)
plt.tight_layout()
plt.show()
