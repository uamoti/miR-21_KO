import os
import numpy as np
import scipy as sp
import pandas as pd
import pandas.api.types as pdt
import matplotlib.pyplot as plt
import matplotlib.patches as mp
import seaborn as sns

path = '~/ownCloud/Doutoramento/microRNA/miR-21/Dados/'
os.chdir(os.path.expanduser(path))

hyd = pd.read_table('hydroxiproline_3k.tsv', header=0, index_col=0, decimal=',')
hyd = hyd[(hyd.genotype == 'dy3k') | (hyd.genotype == 'dko')]
xticks = [r'$dy^{3K}/dy^{3K}$', r'$dy^{3K}$/miR-21']
pal = sns.color_palette(palette=['m', 'r'], n_colors=2)

sns.pointplot(x='genotype', y='regression', data=hyd, join=False, color='k',
              markers=['_', '_'], scale=2, capsize=0.05)
sns.stripplot(x='genotype', y='regression', data=hyd, size=6, palette=pal,
              jitter=True, alpha=0.8)
plt.ylim(0, 0.5)
plt.title('Collagen quantification\nby hydroxyproline assay', fontsize=12)
plt.ylabel('µg collagen/mg muscle', fontsize=12)
plt.xlabel('')
plt.xticks((0, 1), xticks, fontsize=12)

fig = plt.gcf()
fig.dpi = 300
fig.set_size_inches(4, 5)
plt.tight_layout()
plt.show()
