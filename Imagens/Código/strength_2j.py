import os
import numpy as np
import scipy as sp
import pandas as pd
import pandas.api.types as pdt
import matplotlib.pyplot as plt
import matplotlib.patches as mp
import seaborn as sns

path = '~/ownCloud/Doutoramento/microRNA/miR-21/Dados/'
os.chdir(os.path.expanduser(path))

strength = pd.read_table('strength_2j.tsv', header=0, decimal=',')
strength = strength[(strength.genotype == 'dy2j') | (strength.genotype == 'dko')]
xticks = [r'$dy^{2J}/dy^{2J}$', r'$dy^{2J}$/miR-21']
pal = sns.color_palette(palette=['m', 'r'], n_colors=2)

sns.pointplot(x='genotype', y='norm', data=strength, join=False, color='k',
              markers=['_', '_'], scale=2, capsize=0.05)
sns.stripplot(x='genotype', y='norm', data=strength, size=6, palette=pal, alpha=0.8)
plt.ylabel('Normalized grip strength (kgf/g)', fontsize=12)
plt.ylim(0, 0.0065)
plt.xlabel('')
plt.xticks((0, 1), xticks, fontsize=12)

fig = plt.gcf()
fig.dpi = 300
fig.set_size_inches(3, 5)
plt.tight_layout()
plt.show()
