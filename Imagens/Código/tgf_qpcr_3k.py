import os
import numpy as np
import scipy as sp
import pandas as pd
import pandas.api.types as pdt
import matplotlib.pyplot as plt
import matplotlib.patches as mp
import seaborn as sns

path = '~/ownCloud/Doutoramento/microRNA/miR-21/'
os.chdir(os.path.expanduser(path))

tgf3 = pd.read_table('qPCR/tgf_qpcr_3k.tsv', decimal=',', header=0)
cats = pdt.CategoricalDtype(categories=['wt', 'mir-21', 'dy3k', 'dko'], ordered=True)
tgf3.genotyp = tgf3.genotyp.astype(cats)
xticks = ['WT', 'miR-21 ko', '$dy^{3K}/dy^{3K}$', '$dy^{3K}$/miR-21']
n = tgf3.groupby('genotyp').apply(len)
c = ['g', 'y', 'm', 'r']
width = 0.4
err = tgf3.groupby('genotyp').fc.sem()

tgf3.groupby('genotyp').fc.mean().plot.bar(color=c, yerr=err)
plt.ylabel('$Tgfb$ expression'+'\n(fold change vs. WT)')
plt.xticks(range(4), xticks, rotation=45, ha='center')
plt.xlabel('')

for i in range(4):
    plt.text(i, 0.1, str(n[i]), va='center', ha='center')

#fig = plt.gcf()
#fig.dpi = 600
#fig.set_size_inches(4, 4)
plt.tight_layout()
#plt.savefig('Imagens/tgf_3k.svg')
plt.show()
