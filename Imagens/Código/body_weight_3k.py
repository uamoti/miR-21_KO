import os
import numpy as np
import scipy as sp
import pandas as pd
import pandas.api.types as pdt
import matplotlib.pyplot as plt
import matplotlib.patches as mp
import seaborn as sns

path = '~/ownCloud/Doutoramento/microRNA/miR-21/Dados/'
os.chdir(os.path.expanduser(path))

wgt = pd.read_table('body_weight_3k.tsv', header=0, decimal=',')
gen = ['wt', 'mir-21', 'dy3k', 'dko']
cats = pdt.CategoricalDtype(categories=gen, ordered=True)
wgt.genotype = wgt.genotype.astype(cats)
grp = grp.groupby('genotype').weight
means = grp.mean()
sem = grp.sem()
n = grp.apply(len)
x = [1, 2, 3, 4]
width = 0.4
c = ['g', 'y', 'm', 'r']
xticks = ['WT', 'miR-21 ko', r'$dy^{3K}/dy^{3K}$', r'$dy^{3K}$/miR-21']
font = {'size': 12}

plt.bar(x, means, yerr=sem, color=c, tick_label=xticks, alpha=0.9)
plt.ylabel('Body weight (g)', fontsize = 12)
plt.ylim(0, 11)
plt.xticks(fontsize = 12, rotation=45)
plt.text(2, 10, '*', {'size': 16}, va='center', ha='center')
plt.text(3, 5, '***', {'size': 16}, va='center', ha='center')
plt.text(4, 5, '***', {'size': 16}, va='center', ha='center')

for i in range(4):
    plt.text(x[i], 0.5, str(n[i]), va='center', ha='center', fontdict=font)

fig = plt.gcf()
fig.dpi = 300
fig.set_size_inches(4, 6)
plt.tight_layout()
plt.show()
